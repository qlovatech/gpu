package gpu

//Box is a bounding box. Used for frustum culling.
type Box struct {
	Width, Height, Depth float32
}

//Model combines all core GPU elements together ready for rendering.
type Model struct {
	Box
	Transform
	Mesh
	Material
	Animation
}
