module qlova.tech/gpu

go 1.14

require (
	github.com/g3n/engine v0.1.0
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7
	github.com/go-gl/glfw v0.0.0-20200420212212-258d9bec320e
	github.com/go-gl/mathgl v0.0.0-20190713194549-592312d8590a
	gitlab.com/vtgo/gpu v0.0.0-20200707051410-743f6867c863
)
