//Package gpu provides a modern (Physical Based Rendering), high performance GPU renderer for Go.
package gpu

//Viewport is the transform of the camera view.
var Viewport = NewTransform()

//Projection is the camera projection transform.
var Projection = Perspective(60, 1.3, 0.3, 1000)

//Render renders the given model with the given transform.
func Render(model *Model, models []Model, transform *Transform) {
	Default.Render(model, nil, transform)
}

//Render renders the given model with the given transform.
func (b Buffer) Render(model *Model, models []Model, transform *Transform) {
	b.context.Render(model, nil, transform)
}
