package gpu

//Vertices describes the components of a mesh.
type Vertices struct {
	//DrawMode for the vertices.
	Draw DrawMode

	//Vertices data.
	Vertices, Normals []float32

	//Texture mapping data.
	UVs []float32

	//Vertex color data.
	Colors []uint8

	//Animation data.
	Weights []float32
	Joints  []uint8

	//Indices of the vertices, when vertex data is shared.
	Indices []uint32
}

//Mode returns the Mode of the vertices.
func (v Vertices) Mode() Mode {
	var mode Mode
	mode.Draw = v.Draw

	mode.uvs = len(v.UVs) > 0
	mode.indexed = len(v.Indices) > 0
	mode.normals = len(v.Normals) > 0

	return mode
}
