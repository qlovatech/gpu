package gpu

import "errors"

//Default is the default GPU buffer.
var Default Buffer

//Context provides an interface to a context on the GPU.
type Context interface {

	//NewMesh schedules a mesh to be uploaded to the GPU and returns two internal values as a descriptor IE the index and vertex-count.
	NewMesh(Vertices) (index, count uint32)

	//NewMaterial schedules/uploads a material on the GPU and returns references to the four texture maps.
	NewMaterial(Texture) (albedo, physical, normal, emission uint64)

	//NewAnimation schedules an animation to be uploaded to the GPU and returns the index and frame count of the animation.
	NewAnimation(Frames) (index, count uint32)

	//Upload waits for any scheduled meshes, materials and/or animations to upload to the GPU.
	Upload() error

	//Render schedules rendering of the Model or Models with the given transform applied.
	Render(*Model, []Model, *Transform)

	//Sync waits for all scheduled render operations to complete.
	Sync() error
}

//Driver can open a new Context.
//If nil is returned once, it must continue to return nil.
type Driver func() (Context, error)

var driver string
var drivers = make(map[string]Driver)

//RegisterDriver registers a new GPU driver.
func RegisterDriver(name string, d Driver) {
	drivers[name] = d
}

//Open opens the GPU ready for uploading data and rendering.
//You can provide a hint to select the name of the driver to use.
func Open(hints ...string) error {
	if len(hints) > 0 {
		open, ok := drivers[hints[0]]
		if ok {
			ctx, err := open()
			if ctx != nil {
				driver = hints[0]
				Default = Buffer{ctx}
				return nil
			}
			return err
		}
		return errors.New("gpu driver " + hints[0] + " not found")
	}

	if len(drivers) == 0 {
		return errors.New("no drivers available, please import one")
	}

	var ErrorString string = "failed to open a gpu.Context\n"

	for name, open := range drivers {
		ctx, err := open()
		if ctx != nil {
			driver = name
			Default = Buffer{ctx}
			return nil
		}
		ErrorString += "\t" + name + ":" + err.Error()
	}

	return errors.New(ErrorString)
}

//Sync waits for all queued rendering calls to complete.
func Sync() error {
	return Default.Sync()
}

//Upload waits for all queued data to upload to the GPU including meshes, textures and animations.
func Upload() error {
	return Default.Upload()
}
