package gpu

//DrawMode is a primitive draw mode.
type DrawMode struct {
	uint32
}

//DrawModes
var (
	Triangles = DrawMode{1}
	Lines     = DrawMode{2}
)

//Mode describes a mode for rendering a mesh.
type Mode struct {
	Draw DrawMode

	indexed, normals, uvs bool
	skinned, coloured     bool
}

//Indexed returns true if the mesh is using indexed vertices.
func (mode Mode) Indexed() bool {
	return mode.indexed
}
