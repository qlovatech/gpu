package gpu

//Joint defines a current joint transform.
type Joint struct {
	Position, Rotation [3]float32
}

//Frame specifies the position and rotation of each joint.
type Frame []Joint

//Frames is a list of animation frames.
type Frames []Frame

//Animation is a pointer to an animation on the GPU.
type Animation struct {
	index, count uint32

	//if attach is not 0, then just attach to the specified joint minus one.
	joints, attach uint8

	//Frame is the current frame for this instance of the animation.
	//Increment this to play the animation.
	Frame float32
}

//NewAnimation creates a new animation on the GPU using the current GPU driver.
func NewAnimation(frames Frames) Animation {
	return Default.NewAnimation(frames)
}

//NewAnimation creates a new animation on the GPU using the current GPU driver.
func (b Buffer) NewAnimation(frames Frames) Animation {
	if len(frames) == 0 {
		return Animation{}
	}

	index, count := b.context.NewAnimation(frames)

	joints := uint8(len(frames[0]))

	return Animation{index, count, joints, 0, 0}
}
