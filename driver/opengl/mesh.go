package opengl

import (
	"qlova.tech/gpu"
)

import "C"

//NewMesh implements gpu.Driver.NewMesh
func NewMesh(v gpu.Vertices) (index, count uint32) {
	var mode = v.Mode()
	var buffer = BufferFor(mode)

	if mode.Indexed() {
		count = uint32(len(v.Indices))
		index = uint32(len(buffer.VertexPointers))
	} else {
		count = uint32(len(v.Vertices)) / 3
		index = uint32(len(buffer.Vertices))
	}

	buffer.Vertices = append(buffer.Vertices, v.Vertices...)
	buffer.VertexPointers = append(buffer.VertexPointers, v.Indices...)
	buffer.Normals = append(buffer.Normals, v.Normals...)
	buffer.UVs = append(buffer.UVs, v.UVs...)
	buffer.Colors = append(buffer.Colors, v.Colors...)

	return index, count
}

//NewMesh implements gpu.Driver.NewMesh
func (Context) NewMesh(vertices gpu.Vertices) (index, count uint32) {
	return NewMesh(vertices)
}

func (Context) Render(model *gpu.Model, models []gpu.Model, transform *gpu.Transform) {
	Render(model)
	for i := range models {
		Render(&models[i])
	}
}

func Render(model *gpu.Model) {

	var buffer = BufferFor(model.Mode)

	buffer.Transforms = append(buffer.Transforms, model.Transform)

	if model.Indexed() {
		buffer.DrawElementCommands = append(buffer.DrawElementCommands, DrawElementsIndirectCommand{
			C.uint(model.Count()),
			1,
			C.uint(model.Index()),
			0,
			0,
		})
	} else {
		buffer.DrawArrayCommands = append(buffer.DrawArrayCommands, DrawArraysIndirectCommand{
			C.uint(model.Count()),
			1,
			C.uint(model.Index()),
			0,
		})
	}

	if albedo := model.AlbedoMap(); albedo != 0 {
		buffer.ColorMaps = append(buffer.ColorMaps, albedo-1)
	}
}
