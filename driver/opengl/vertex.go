package opengl

const Vertex = `
in vec4 vertex;
in vec3 normal;
in vec2 uv;
in vec4 color;

in uvec4 weight;
in uvec4 joint;

out vec4 blend;

layout(std140) uniform transform {
	mat4 transforms[MAX_UNIFORM_BLOCK_SIZE/256];
};

uniform mat4 viewport;
uniform mat4 projection;

void main() {
	int i = gl_DrawID;

	mat4 view = viewport * transforms[i];

	mat4 mvp = projection * view;
	mat4 finalWorld = mat4(1.0);

	gl_Position =  mvp * finalWorld * vertex;
	
	blend = color;
}
`

const VertexFallback = `
in vec4 vertex;
in vec3 normal;
in vec2 uv;
in uvec3 weights;

layout(std140) uniform transform {
	mat4 transforms[MAX_UNIFORM_BLOCK_SIZE/256];
};

uniform mat4 viewport;
uniform mat4 projection;

uniform int drawID;

out vec3 vposition;
out vec3 vnormal;
out vec2 vuv;
flat out int i;

void main() {
	i =  drawID+gl_InstanceID;
	
	mat4 view = viewport * transforms[i];
	mat4 matrix = transpose(inverse(view));

	vposition = (view * vertex).xyz;
	vnormal = normalize(vec3(matrix*vec4(normal, 0.0)));

	mat4 mvp = projection * view;
	mat4 finalWorld = mat4(1.0);

	vuv = uv;

    gl_Position =  mvp * finalWorld * vertex;
}
`
