package opengl

import (
	"errors"
	"fmt"

	"qlova.tech/gpu"

	"github.com/g3n/engine/gls"
	"github.com/go-gl/gl/v4.6-core/gl"
)

//BufferFor returns the buffer for this Mode.
func BufferFor(m gpu.Mode) *Buffer {
	if buffer, ok := buffers[m]; ok {
		return buffer
	}
	buffer := new(Buffer)
	buffers[m] = buffer
	return buffer
}

var buffers = make(map[gpu.Mode]*Buffer)

type Buffer struct {
	//Buffer pointers on the GPU.
	vertex, normals, uvs, vertexpointers, colors, draw uint32

	//Data to buffer.
	Vertices, Normals, UVs []float32
	VertexPointers         []uint32
	Colors                 []uint8

	//There are 3 4x4 matrices.
	Transforms []gpu.Transform
	transforms uint32

	//Textures.
	ColorMaps []uint64
	colormaps uint32

	//Rendering queue.
	DrawArrayCommands   []DrawArraysIndirectCommand
	DrawElementCommands []DrawElementsIndirectCommand

	commands uint32

	//Vertex Attributes Object pointer on the GPU.
	attributes uint32
}

func (buffer *Buffer) upload(mode gpu.Mode) error {
	var attributes uint32
	var CreatingAttributes bool = buffer.attributes == 0

	if CreatingAttributes {
		gl.GenVertexArrays(1, &attributes)
		gl.BindVertexArray(attributes)
	}

	if buffer.vertex == 0 {
		gl.GenBuffers(1, &buffer.vertex)
	}
	gl.BindBuffer(gls.ARRAY_BUFFER, buffer.vertex)
	gl.BufferData(gls.ARRAY_BUFFER, len(buffer.Vertices)*4, gl.Ptr(buffer.Vertices), gls.STATIC_DRAW)
	if CreatingAttributes {
		gl.EnableVertexAttribArray(vertex)
		gl.VertexAttribPointer(vertex, 3, gls.FLOAT, false, 0, nil)
	}

	if buffer.Colors != nil {
		if len(buffer.Colors) != (len(buffer.Vertices)/3)*4 {
			fmt.Println(len(buffer.Colors), (len(buffer.Vertices)/3)*4)
			return errors.New("length of colors doesn't match length of vertices")
		}

		if buffer.colors == 0 {
			gl.GenBuffers(1, &buffer.colors)
		}
		gl.BindBuffer(gls.ARRAY_BUFFER, buffer.colors)
		gl.BufferData(gls.ARRAY_BUFFER, len(buffer.Colors), gl.Ptr(buffer.Colors), gls.STATIC_DRAW)

		if CreatingAttributes {
			gl.EnableVertexAttribArray(color)
			gl.VertexAttribPointer(color, 4, gls.UNSIGNED_BYTE, true, 0, nil)
		}
	}

	if buffer.Normals != nil {
		fmt.Println("adding normals to buffer")
		if len(buffer.Normals) != len(buffer.Vertices) {
			return errors.New("length of normals doesn't match length of vertices")
		}

		if buffer.normals == 0 {
			gl.GenBuffers(1, &buffer.normals)
		}
		gl.BindBuffer(gls.ARRAY_BUFFER, buffer.normals)
		gl.BufferData(gls.ARRAY_BUFFER, len(buffer.Normals)*4, gl.Ptr(buffer.Normals), gls.STATIC_DRAW)

		if CreatingAttributes {
			gl.EnableVertexAttribArray(normal)
			gl.VertexAttribPointer(normal, 3, gls.FLOAT, false, 0, nil)
		}
	}

	if buffer.UVs != nil {
		if len(buffer.UVs) != (len(buffer.Vertices)/3)*2 {
			return errors.New("length of uvs doesn't match length of vertices")
		}

		if buffer.uvs == 0 {
			gl.GenBuffers(1, &buffer.uvs)
		}
		gl.BindBuffer(gls.ARRAY_BUFFER, buffer.uvs)
		gl.BufferData(gls.ARRAY_BUFFER, len(buffer.UVs)*4, gl.Ptr(buffer.UVs), gls.STATIC_DRAW)

		if CreatingAttributes {
			gl.EnableVertexAttribArray(uv)
			gl.VertexAttribPointer(uv, 2, gls.FLOAT, false, 0, nil)
		}
	}

	if buffer.VertexPointers != nil {
		if buffer.vertexpointers == 0 {
			gl.GenBuffers(1, &buffer.vertexpointers)
		}
		gl.BindBuffer(gls.ELEMENT_ARRAY_BUFFER, buffer.vertexpointers)
		gl.BufferData(gls.ELEMENT_ARRAY_BUFFER, len(buffer.VertexPointers)*4, gl.Ptr(buffer.VertexPointers), gls.STATIC_DRAW)
	}

	gl.BindVertexArray(0)

	if CreatingAttributes {
		buffer.attributes = attributes
	}

	//Reset for the next upload
	buffer.Vertices = nil
	buffer.VertexPointers = nil
	buffer.Normals = nil
	buffer.UVs = nil
	buffer.Colors = nil

	var count int
	for {
		err := gl.GetError()
		if err == 0 {
			break
		}
		if err != 0 {
			count++
		}
	}

	if count > 0 {
		return fmt.Errorf("%v gl error(s)", count)
	}

	return nil
}

//Upload uploads all queued data to the GPU ready for rendering.
func Upload() error {
	for mode, buffer := range buffers {
		if err := buffer.upload(mode); err != nil {
			return err
		}
	}

	return nil
}
