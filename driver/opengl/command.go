package opengl

/*
typedef unsigned int uint;

typedef  struct {
	uint count;
	uint instanceCount;
	uint firstIndex;
	uint baseVertex;
	uint baseInstance;
} DrawElementsIndirectCommand;

typedef  struct {
	uint  count;
	uint  instanceCount;
	uint  first;
	uint  baseInstance;
} DrawArraysIndirectCommand;
*/
import "C"

type DrawElementsIndirectCommand = C.DrawElementsIndirectCommand

type DrawArraysIndirectCommand = C.DrawArraysIndirectCommand
