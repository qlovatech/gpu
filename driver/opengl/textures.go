package opengl

import "qlova.tech/gpu"

//NewMaterial implements gpu.Driver.NewMaterial
func (Context) NewMaterial(texture gpu.Texture) (albedo, physical, normal, emission uint64) {
	/*switch img.(type) {
	case *image.NRGBA:
	default:
		return 0, fmt.Errorf("textures of type %v are not supported by this driver", reflect.TypeOf(img))
	}

	var rgba = img.(*image.NRGBA)

	var w, h = img.Bounds().Max.X, img.Bounds().Max.Y

	if gl.GetError() != 0 {
		return 0, errors.New("previous gl error")
	}

	var texture uint32
	gl.GenTextures(1, &texture)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D_ARRAY, texture)
	if gl.GetError() != 0 {
		return 0, errors.New("BindTexture error")
	}
	gl.TexImage3D(gl.TEXTURE_2D_ARRAY,
		0,
		gl.RGB8,
		int32(w), int32(h),
		1,
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		nil,
	)
	if err := gl.GetError(); err != 0 {
		return 0, fmt.Errorf("TexImage3D error: %v", err)
	}

	gl.TexSubImage3D(gl.TEXTURE_2D_ARRAY,
		0,
		0, 0, 0,
		int32(w), int32(h), 1,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(rgba.Pix),
	)
	if gl.GetError() != 0 {
		return 0, errors.New("TexSubImage3D error")
	}

	gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D_ARRAY, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)

	return 1, nil*/
	return 0, 0, 0, 0
}
