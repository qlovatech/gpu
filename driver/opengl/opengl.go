package opengl

import (
	"fmt"

	"qlova.tech/gpu"

	"github.com/go-gl/gl/v4.6-core/gl"
)

//uniforms
var viewport, projection, drawID int32
var sun, sunlight, darkness int32
var colormap, samplers int32

//vertex attributes
var vertex, normal, uv, weights, color uint32

var DefaultProgram Program

func init() {
	gpu.RegisterDriver("opengl", Open)
}

//fallback is true if we need to fallback to OpenGL 4.0
var fallback bool

var (
	maxTextureSize,
	maxUniformBlockSize,
	maxVertexTextureUnits,
	maxArrayTextureLayers int32
)

var opened bool

//Open returns an OpenGL gpu.Context
func Open() (gpu.Context, error) {
	if opened {
		return Context{}, nil
	}

	if err := gl.Init(); err != nil {
		return nil, err
	}

	//Check version
	var major, minor int32
	gl.GetIntegerv(gl.MAJOR_VERSION, &major)
	gl.GetIntegerv(gl.MINOR_VERSION, &minor)

	if major < 4 || (major == 4 && minor < 6) {
		fallback = true
	}

	//Query capabillities
	gl.GetIntegerv(gl.MAX_UNIFORM_BLOCK_SIZE, &maxUniformBlockSize)
	gl.GetIntegerv(gl.MAX_ARRAY_TEXTURE_LAYERS, &maxArrayTextureLayers)
	gl.GetIntegerv(gl.MAX_TEXTURE_SIZE, &maxTextureSize)
	gl.GetIntegerv(gl.MAX_VERTEX_TEXTURE_IMAGE_UNITS, &maxVertexTextureUnits)

	fmt.Println(maxUniformBlockSize)

	//Setup shader
	fmt.Printf("Running on OpenGL %v.%v\n", major, minor)

	var Defines = fmt.Sprintf(`
#define MAX_UNIFORM_BLOCK_SIZE %v
#define MAX_TEXTURE_UNITS %v
`, maxUniformBlockSize, maxVertexTextureUnits)

	DefaultProgram = NewProgram()
	if fallback {
		DefaultProgram.Shaders = append(DefaultProgram.Shaders,
			FragmentShader("#version 330\n"+Defines+FragmentFallback))
		DefaultProgram.Shaders = append(DefaultProgram.Shaders,
			VertexShader("#version 330\n"+Defines+VertexFallback))
	} else {
		DefaultProgram.Shaders = append(DefaultProgram.Shaders,
			FragmentShader("#version 460 core\n"+Defines+Fragment))
		DefaultProgram.Shaders = append(DefaultProgram.Shaders,
			VertexShader("#version 460 core\n"+Defines+Vertex))
	}

	if err := DefaultProgram.Build(); err != nil {
		return nil, err
	}

	if err := gl.GetError(); err != 0 {
		return nil, fmt.Errorf("gl error: %v", err)
	}

	//Load uniforms.
	viewport = gl.GetUniformLocation(DefaultProgram.pointer, gl.Str("viewport\000"))
	projection = gl.GetUniformLocation(DefaultProgram.pointer, gl.Str("projection\000"))
	sun = gl.GetUniformLocation(DefaultProgram.pointer, gl.Str("sun\000"))
	sunlight = gl.GetUniformLocation(DefaultProgram.pointer, gl.Str("sunlight\000"))
	darkness = gl.GetUniformLocation(DefaultProgram.pointer, gl.Str("darkness\000"))
	samplers = gl.GetUniformLocation(DefaultProgram.pointer, gl.Str("samplers\000"))
	if err := gl.GetError(); err != 0 {
		return nil, fmt.Errorf("gl error: %v", err)
	}

	if fallback {
		drawID = gl.GetUniformLocation(DefaultProgram.pointer, gl.Str("drawID\000"))
	}

	//Uniform blocks.
	gl.UniformBlockBinding(DefaultProgram.pointer,
		gl.GetUniformBlockIndex(DefaultProgram.pointer, gl.Str("transform\000")),
		transformUniformBlockBinding,
	)

	if fallback {
		gl.UniformBlockBinding(DefaultProgram.pointer,
			gl.GetUniformBlockIndex(DefaultProgram.pointer, gl.Str("colormap\000")),
			colormapUniformBlockBinding,
		)
	}

	//Load vertex attributes
	vertex = uint32(gl.GetAttribLocation(DefaultProgram.pointer, gl.Str("vertex\000")))
	normal = uint32(gl.GetAttribLocation(DefaultProgram.pointer, gl.Str("normal\000")))
	color = uint32(gl.GetAttribLocation(DefaultProgram.pointer, gl.Str("color\000")))
	uv = uint32(gl.GetAttribLocation(DefaultProgram.pointer, gl.Str("uv\000")))
	weights = uint32(gl.GetAttribLocation(DefaultProgram.pointer, gl.Str("weights\000")))

	//Setup opengl mode.

	gl.UseProgram(DefaultProgram.pointer)

	gl.ClearColor(0, 0, 0, 1)
	gl.ClearDepth(1)
	gl.ClearStencil(0)
	gl.Enable(gl.DEPTH_TEST)
	gl.DepthFunc(gl.LEQUAL)
	gl.CullFace(gl.BACK)
	gl.Enable(gl.CULL_FACE)

	var count int
	var errors []uint32
	for {
		err := gl.GetError()
		if err == 0 {
			break
		}
		if err != 0 {
			count++
		}
		errors = append(errors, err)
	}

	if count > 0 {
		return nil, fmt.Errorf("%v gl error(s): %v", count, errors)
	}

	opened = true

	return Context{}, nil
}

const (
	transformUniformBlockBinding = iota
	colormapUniformBlockBinding
)

//Context is an OpenGL driver interface to the GPU implementing the gpu.GPU interface.
type Context struct {
	buffers map[gpu.Mode]*Buffer
}

func (Context) Sync() error {
	return Sync()
}

func (Context) Upload() error {
	return Upload()
}

func Sync() error {
	for mode, buffer := range buffers {
		if err := buffer.sync(mode); err != nil {
			return err
		}
	}
	return nil
}
