package opengl

import (
	"fmt"
	"strings"

	"github.com/go-gl/gl/v4.6-core/gl"
)

type Program struct {
	pointer uint32

	Shaders []Shader
}

func NewProgram() Program {
	return Program{gl.CreateProgram(), nil}
}

func (p Program) Use() {
	gl.UseProgram(p.pointer)
}

func (p Program) Build() error {

	var compiledShaders []CompiledShader

	for _, shader := range p.Shaders {

		compiled, err := shader.Compile()
		if err != nil {
			return err
		}

		compiledShaders = append(compiledShaders, compiled)

		gl.AttachShader(p.pointer, uint32(compiled))
	}

	gl.LinkProgram(p.pointer)

	var status int32
	gl.GetProgramiv(p.pointer, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(p.pointer, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(p.pointer, logLength, nil, gl.Str(log))

		return fmt.Errorf("failed to link program: %v", log)
	}

	for _, compiled := range compiledShaders {
		gl.DeleteShader(uint32(compiled))
	}

	return nil
}

type CompiledShader uint32

type Shader interface {
	Compile() (CompiledShader, error)
}

type FragmentShader string

func (s FragmentShader) Compile() (CompiledShader, error) {
	shader := gl.CreateShader(gl.FRAGMENT_SHADER)

	csources, free := gl.Strs(string(s))
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile fragment shader: %v", log)
	}

	return CompiledShader(shader), nil
}

type VertexShader string

func (s VertexShader) Compile() (CompiledShader, error) {
	shader := gl.CreateShader(gl.VERTEX_SHADER)

	csources, free := gl.Strs(string(s) + "\000")
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile vertex shader: %v", log)
	}

	return CompiledShader(shader), nil
}
