package opengl

import (
	"fmt"
	"unsafe"

	"qlova.tech/gpu"

	"github.com/g3n/engine/gls"

	"github.com/go-gl/gl/v4.6-core/gl"
)

//sync draws the buffer in a single draw call.
func (buffer *Buffer) sync(mode gpu.Mode) error {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT | gl.STENCIL_BUFFER_BIT)

	gl.BindVertexArray(buffer.attributes)
	defer gl.BindVertexArray(0)

	//upload transforms.
	if buffer.transforms == 0 {
		gl.GenBuffers(1, &buffer.transforms)
	}
	gl.BindBuffer(gls.UNIFORM_BUFFER, buffer.transforms)
	gl.BufferData(gls.UNIFORM_BUFFER, len(buffer.Transforms)*4*4*4, gl.Ptr(buffer.Transforms), gls.STATIC_DRAW)
	gl.BindBufferRange(gls.UNIFORM_BUFFER, transformUniformBlockBinding, buffer.transforms, 0, len(buffer.Transforms)*4*4*4)

	//upload textures.
	/*if buffer.colormaps == 0 {
		gl.GenBuffers(1, &buffer.colormaps)
	}
	gl.BindBuffer(gls.UNIFORM_BUFFER, buffer.colormaps)
	gl.BufferData(gls.UNIFORM_BUFFER, len(buffer.ColorMaps)*4*2, gl.Ptr(buffer.ColorMaps), gls.STATIC_DRAW)
	gl.BindBufferRange(gls.UNIFORM_BUFFER, colormapUniformBlockBinding, buffer.colormaps, 0, len(buffer.ColorMaps)*4*2)*/

	//fmt.Println(buffer.Vertices, buffer.VertexPointers)

	//Fallback renderer.
	/*if mode.indexed {
		for _, command := range buffer.DrawElementCommands {
			gl.DrawElements(mode.draw, int32(command.Count), gls.UNSIGNED_INT, uint32(command.FirstIndex))
		}
	} else {
		for _, command := range buffer.DrawArrayCommands {
			gl.DrawArrays(mode.draw, int32(command.First), int32(command.Count))
		}
	}*/

	gl.UniformMatrix4fv(viewport, 1, false, &gpu.Viewport[0])
	gl.UniformMatrix4fv(projection, 1, false, &gpu.Projection[0])

	gl.Uniform3fv(sun, 1, &gpu.Sun.Transform[12])
	gl.Uniform3f(sunlight, 1, 1, 1)
	gl.Uniform3f(darkness, 0.1, 0.1, 0.1)

	gl.Uniform1i(samplers, 0)

	var drawmode uint32

	if mode.Draw == gpu.Triangles {
		drawmode = gl.TRIANGLES
	}

	if mode.Draw == gpu.Lines {
		drawmode = gl.LINES
	}

	if fallback {

		var id int32
		for _, command := range buffer.DrawElementCommands {
			gl.Uniform1i(drawID, id)
			gl.DrawElementsInstanced(drawmode, int32(command.count), gls.UNSIGNED_INT, gl.Ptr(command.firstIndex), int32(command.instanceCount))
			id += int32(command.instanceCount)
		}
		for _, command := range buffer.DrawArrayCommands {
			gl.Uniform1i(drawID, id)
			gl.DrawArraysInstanced(drawmode, int32(command.first), int32(command.count), int32(command.instanceCount))
			id += int32(command.instanceCount)
		}

		buffer.DrawElementCommands = buffer.DrawElementCommands[0:0:cap(buffer.DrawElementCommands)]
		buffer.DrawArrayCommands = buffer.DrawArrayCommands[0:0:cap(buffer.DrawArrayCommands)]
	} else {
		if mode.Indexed() {
			if buffer.commands == 0 {
				gl.GenBuffers(1, &buffer.commands)
			}
			gl.BindBuffer(gl.DRAW_INDIRECT_BUFFER, buffer.commands)
			gl.BufferData(gl.DRAW_INDIRECT_BUFFER,
				len(buffer.DrawElementCommands)*int(unsafe.Sizeof(DrawElementsIndirectCommand{})),
				gl.Ptr(buffer.DrawElementCommands),
				gls.STATIC_DRAW)

			gl.MultiDrawElementsIndirect(drawmode, gls.UNSIGNED_INT, nil, int32(len(buffer.DrawElementCommands)), 0)
			buffer.DrawElementCommands = buffer.DrawElementCommands[0:0:cap(buffer.DrawElementCommands)]
		} else {

			if buffer.commands == 0 {
				gl.GenBuffers(1, &buffer.commands)
			}
			gl.BindBuffer(gl.DRAW_INDIRECT_BUFFER, buffer.commands)
			gl.BufferData(gl.DRAW_INDIRECT_BUFFER,
				len(buffer.DrawArrayCommands)*int(unsafe.Sizeof(DrawArraysIndirectCommand{})),
				gl.Ptr(buffer.DrawArrayCommands),
				gls.STATIC_DRAW)

			gl.MultiDrawArraysIndirect(drawmode, nil, int32(len(buffer.DrawArrayCommands)), 0)
			buffer.DrawArrayCommands = buffer.DrawArrayCommands[0:0:cap(buffer.DrawArrayCommands)]
		}
	}

	//reset commands and transforms for the next frame.
	buffer.Transforms = buffer.Transforms[0:0:cap(buffer.Transforms)]
	buffer.ColorMaps = buffer.ColorMaps[0:0:cap(buffer.ColorMaps)]

	var count int
	for {
		err := gl.GetError()

		if err == 0 {
			break
		}
		if err != 0 {
			fmt.Println(err)
			count++
		}
	}

	if count > 0 {
		return fmt.Errorf("%v gl error(s)", count)
	}

	return nil
}
