package gpu

import "image"

//Texture is a texture that can be used to create a material on the GPU.
type Texture struct {
	image.Image

	PhysicalMap, NormalMap, EmissionMap image.Image
}
