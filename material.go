package gpu

//Material is a Material on the GPU.
type Material struct {
	albedo, physical, normal, emission uint64
}

//NewMaterial creates a new material on the GPU using the current GPU driver.
func NewMaterial(texture Texture) Material {
	return Default.NewMaterial(texture)
}

//NewMaterial creates a new material on the GPU using the current GPU driver.
func (b Buffer) NewMaterial(texture Texture) Material {
	albedo, physical, normal, emission := b.context.NewMaterial(texture)
	return Material{albedo, physical, normal, emission}
}

//AlbedoMap returns the internal gpu representation for the albedo map of this material.
func (m Material) AlbedoMap() uint64 {
	return m.albedo
}
