package gpu

import (
	"github.com/g3n/engine/math32"
	"github.com/go-gl/mathgl/mgl32"
)

//effective members of a Transform
const (
	x = 12
	y = 13
	z = 14
)

//Origin is the position 0,0,0
var Origin = NewTransform()

//Transform describes the position, orientation and scale to apply to a target.
type Transform [16]float32

//NewTransform returns a new indentity transform that does not transform the target in any way.
func NewTransform() Transform {
	return Transform{1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1}
}

//Position returns a new transform that translates the target to the given x,y,z coordinates.
func Position(X, Y, Z float32) Transform {
	var t = NewTransform()
	t.SetPosition(X, Y, Z)
	return t
}

//Perspective returns a new perspective transform.
func Perspective(fov, aspect, near, far float32) Transform {
	return Transform(*math32.NewMatrix4().MakePerspective(fov, aspect, near, far))
}

//SetPosition sets the transform to translate the target to the given x,y,z coordinates.
func (t *Transform) SetPosition(X, Y, Z float32) {
	t[x] = X
	t[y] = Y
	t[z] = Z
}

//Translate translates the transfrom by the given x,y,z coordinates.
func (t *Transform) Translate(X, Y, Z float32) {
	t[x] += X
	t[y] += Y
	t[z] += Z
}

//Position returns the xyz translation component of the transform.
func (t Transform) Position() (X, Y, Z float32) {
	return t[x], t[y], t[z]
}

//LookAt returns a transform that looks from t to the target positions.
func (t Transform) LookAt(target Transform) Transform {

	newVector := func(x, y, z float32) mgl32.Vec3 {
		return mgl32.Vec3{x, y, z}
	}

	eye := newVector(t.Position())
	up := newVector(0, 1, 0)
	center := newVector(target.Position())

	return Transform(mgl32.LookAtV(eye, center, up))
}
