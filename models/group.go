package models

import (
	"qlova.tech/gpu"
)

type Group struct {
	gpu.Transform

	small  bool
	buffer [5]gpu.Model
	slice  []gpu.Model
}
