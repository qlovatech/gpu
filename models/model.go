package models

import (
	"fmt"
	"path"

	"qlova.tech/gpu"
)

type Opener func(name string, into []gpu.Model) error

var registered = make(map[string]Opener)

func RegisterFormat(ext string, opener Opener) {
	registered[ext] = opener
}

//Open opens the provided 3D model file and tries to load models into the provided interface.
//Valid interface types are *gpu.Model, []gpu.Model
func Open(name string, into []gpu.Model) error {
	ext := path.Ext(name)
	opener, ok := registered[ext]
	if ok {
		opener(name, into)
		return nil
	}
	return fmt.Errorf("unrecognised format: %v", ext)
}

/*var Textures = map[string]gpu.Texture{}

//Texture loads or returns the loaded texture for the given path.
func Texture(name string) (gpu.Texture, error) {
	if tex, ok := Textures[name]; ok {
		return tex, nil
	}

	f, err := os.Open(name)
	if err != nil {
		return 0, err
	}

	img, _, err := image.Decode(f)
	if err != nil {
		return 0, err
	}

	tex, err := gpu.NewTexture(img)
	if err != nil {
		return tex, err
	}

	Textures[name] = tex

	return tex, nil
}
*/
