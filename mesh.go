package gpu

//Mesh is a group of vertices on the GPU.
type Mesh struct {
	Mode

	index, count uint32
}

//NewMesh returns a new mesh on the GPU from the given vertices.
func NewMesh(vertices Vertices) Mesh {
	return Default.NewMesh(vertices)
}

//NewMesh returns a new mesh on the GPU from the given vertices.
func (b Buffer) NewMesh(vertices Vertices) Mesh {
	index, count := b.context.NewMesh(vertices)
	return Mesh{vertices.Mode(), index, count}
}

//Index returns the internal gpu index for this mesh.
func (m Mesh) Index() uint32 {
	return m.index
}

//Count returns the number of vertices or indicies of this mesh.
func (m Mesh) Count() uint32 {
	return m.count
}

//Model returns a new model with this Mesh as its Mesh.
func (m Mesh) Model() *Model {
	return &Model{
		Transform: NewTransform(),
		Mesh:      m,
	}
}
