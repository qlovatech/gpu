package gpu

import "image/color"

//Darkness is the color of dark objects, things cannot get darker than this.
//AKA Ambient Light
var Darkness color.Color

//Sun is the base directional light of the scene.
var Sun Light

//Light is either a spot light or point light depending on the cuttoff Angle of the light.
type Light struct {
	Transform
	Angle float32
	Color [3]float32
}
