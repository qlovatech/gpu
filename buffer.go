package gpu

//Buffer that can be used to control what data is currently available to the GPU.
type Buffer struct {
	context Context
}

//NewBuffer creates and returns a new buffer ready for use.
func NewBuffer() Buffer {
	ctx, err := drivers[driver]()
	if err != nil {
		panic("NewBuffer() called before Open()")
	}
	return Buffer{ctx}
}

//Sync waits for all queued rendering calls to complete.
func (b Buffer) Sync() error {
	return b.context.Sync()
}

//Upload waits for all queued data to upload to the GPU including meshes, textures and animations.
func (b Buffer) Upload() error {
	return b.context.Upload()
}
